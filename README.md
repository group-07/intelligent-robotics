Repository used for the Intelligence Robotics module 2021-2022.

Team members:
- Andreea-Bianca Fraunhoffer
- Florian-Andrei Blanaru
- Rahul Gheewala
- Yuji Fukuta
- Samuel Irsai


1. First assignment (Particle filter localisation).

Reqirement
- pip install shapely


##TO RUN THE CODE

*make sure you rename the file to "intelligent-robotics"*

python3 src/assistantwithspeech.py

roslaunch intelligent-robotics start.launch


or 

- roscore
- rosrun stage_ros stageros catkin_ws/src/intelligent-robotics/data/real_data/CityMap.world
- rosrun map_server map_server catkin_ws/src/intelligent-robotics/data/real_data/CityMap.yaml
- rosrun intelligent-robotics node.py
- rosrun rviz rviz
- python3 catkin_ws/src/intelligent-robotics/a_star/a_star_search


Report for the first assignment: https://docs.google.com/document/d/10ouy3Q-iUCVibC8MZmafSC4_6c0A5V1BUCjSgPjox8E/edit?usp=sharing

Steps for using assistant:
1. Make sure you are using the PyCharm IDE
2. install pyttsx3 import
3. install pyaudio import
4. pip install SpeechRecognition
5. sudo apt-get install portaudio19-dev python3-pyaudio
6. sudo apt install espeak
7. Make sure audio input is enabled in VM
