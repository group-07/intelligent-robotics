import pyttsx3
import speech_recognition as sr
import pyaudio

#########################
r = sr.Recognizer()

global my_text
my_text = ""
items = []


def get_audio():
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source, duration=0.2)
        audio = r.listen(source, phrase_time_limit=5)

        try:
            global my_text
            my_text = r.recognize_google(audio)

        except:

            bot_statement("Sorry could not recognize your voice, please try again!")
            print('Please speak...')
            get_audio()


#########################
engine = pyttsx3.init()


# option_one = '1' in words or 'one' in words or 'first' in words or '1st'
# option_two = '2' in words or 'two' in words or 'second' in words or '2nd'
# option_three = '3' in words or 'three' in words or 'third' in words or '3rd'


def bot_statement(output):
    print("[ASSISTANT]: " + output)
    engine.say(output)
    engine.runAndWait()


def user_statement(output):
    print("[USER]: " + output)


def take_option():
    print("Please speak...")
    get_audio()
    global words
    words = my_text.split()
    user_statement(my_text)


def choose_user_option():
    # option = input("Please select an option: ")
    take_option()

    if '1' in words or 'one' in words or 'first' in words or '1st' in words or 'customer' in words:
        bot_statement('Hello, and welcome to our restaurant. Thank you for using our delivery service, '
                      'please reply with the service which you require. Option 1, make a new order. Option 2, '
                      'employee options. Press 3 to test out speech to voice.')
        choose_customer_option()
    elif '2' in words or 'two' in words or 'second' in words or '2nd' in words or 'employee' in words:
        bot_statement('Hello employee, welcome back to the system! Would you like to deliver an order, Option 1 or '
                      'clean the kitchen, Option 2 ')
        choose_employee_option()
    elif '3' in words or 'three' in words or 'third' in words or '3rd' in words:
        bot_statement("Please say something to the mic, if successful, it should be read back to you.")
        get_audio()
        bot_statement("Did you say: " + my_text + "?")
    else:
        bot_statement('You chose an invalid option, please try again!')
        choose_user_option()


def choose_customer_option():
    # option = input("Please select an option: ")
    take_option()

    if '1' in words or 'one' in words or 'first' in words or '1st' in words:
        bot_statement('Would you like a drink?')
        # user_want_drinks = input('Drink? ')
        take_option()
        want_drink(words)
    elif '2' in words or 'two' in words or 'second' in words or '2nd' in words:
        bot_statement('Here is the status of your order: ')
    else:
        bot_statement('You chose an invalid option, please try again!')
        choose_customer_option()


def want_drink(option):
    if 'yes' in words or 'yep' in words or 'yeah' in words:
        choose_drink()
    elif 'no' in words or 'nah' in words or 'nope' in words:
        choose_food()
    else:
        bot_statement("Please reply with yes or no")
        want_drink(input('Would you like a drink? '))


drinks = ['Coke', 'Pepsi', 'sprite', 'Fanta', 'tango',
          'water']


def choose_drink():
    bot_statement('We have Coke, Pepsi, Sprite, Fanta, Tango, Water. Which would you like?')
    # drink_input = input("Choose your drink: ")
    take_option()

    drink = ""

    for word in words:
        if word in drinks:
            drink = word
            items.append(drink)

    if drink != "":
        bot_statement('You chose ' + drink + ', an excellent choice. Now please choose a sandwich.')
        choose_food()
    else:
        bot_statement("We do not have this drink, please choose another one!")
        choose_drink()


breads = ['tiger', 'Italian', 'white', 'brown']
fillings = ['tuna', 'chicken', 'Turkey', 'beef', 'vegetables']


def choose_food():
    bot_statement('Please choose a bread, we have:  Tiger, Italian, White and Brown')
    # bread_input = input("Choose your bread: ")

    take_option()

    bread = ""

    for word in words:
        if word in breads:
            bread = word
            items.append(bread)

    if bread != "":
        bot_statement('You chose ' + bread + '. Now please choose a filling. We have Tuna, Chicken, Turkey, '
                                             'Beef and Vegetables.')
        choose_filling()
    else:
        bot_statement("We do not have this bread, please choose another one!")
        choose_food()


def choose_filling():
    #   filling_input = input("Choose your filling: ")

    take_option()

    filling = ""

    for word in words:
        if word in fillings:
            filling = word
            items.append(filling)

    if filling != "":
        bot_statement('You chose ' + filling + '. Would you like another filling?.')
        take_option()
        if 'yes' in words or 'yep' in words or 'yeah' in words:
            bot_statement("What additional filling would you like?")
            choose_filling()

        elif 'no' in words or 'nah' in words or 'nope' in words:
            bot_statement("Your sandwich is complete. Please enter your address for delivery")
            address = input("Please enter your address: ")
            f = open("../data/address.txt", "w")
            f.write(address)
            f.close()

            if items[0] in drinks:
                order_message_file = open("../data/order_message_file.txt", "w")
                order_message_file.write('Your order has been delivered. Your drink is ' + items[
                    0] + ',' 'your bread choice is ' + items[
                                             1] + ' and your fillings are ' + " and ".join(items[2:]))
                bot_statement('Thank you! You should receive your order very soon. Your drink is ' + items[
                    0] + ',' 'your bread choice is ' + items[
                                  1] + ' and your fillings are ' + " and ".join(items[2:]))
            else:
                order_message_file = open("data/order_message_file.txt", "w")
                order_message_file.write('Your order has been delivered. Your drink is ' + items[
                    0] + ',' 'your bread choice is ' + items[
                                             1] + ' and your fillings are ' + " and ".join(items[2:]))
                bot_statement('Thank you! You should receive your order very soon. Your bread choice is ' + items[
                    0] + ' and your fillings are ' + " and ".join(items[1:]))

        else:
            bot_statement("Please reply with yes or no")
            more_fillings(take_option())

    else:
        bot_statement("We do not have this filling, please choose another one!")
        choose_filling()


def more_fillings(option):
    if 'yes' in words or 'yep' in words or 'yeah' in words:
        bot_statement("What additional filling would you like?")
        choose_filling()
    elif 'no' in words or 'nah' in words or 'nope' in words:
        bot_statement("Your sandwich is complete. Please enter your address for delivery")
        address = input("Please enter your address: ")
        f = open("../data/address.txt", "w")
        f.write(address)
        f.close()

        if items[0] in drinks:
            order_message_file = open("data/order_message_file.txt", "w")
            order_message_file.write('Your order has been delivered. Your drink is ' + items[
                0] + ',' 'your bread choice is ' + items[
                                         1] + ' and your fillings are ' + " and ".join(items[2:]))
            bot_statement('Thank you! You should receive your order very soon. Your drink is ' + items[0] + ',' 'your bread choice is ' + items[
                1] + ' and your fillings are ' + " and ".join(items[2:]))
        else:
            order_message_file = open("data/order_message_file.txt", "w")
            order_message_file.write('Your order has been delivered. Your drink is ' + items[
                0] + ',' 'your bread choice is ' + items[
                                         1] + ' and your fillings are ' + " and ".join(items[2:]))
            bot_statement('Thank you! You should receive your order very soon. Your bread choice is ' + items[0] + ' and your fillings are ' + " and ".join(items[1:]))

    else:
        bot_statement("Please reply with yes or no")
        more_fillings(take_option())


def choose_employee_option():
    # option = input("Please select an option: ")

    take_option()

    if '1' in words or 'one' in words or 'first' in words or '1st' in words:
        bot_statement('You have chosen to Deliver an Order')
    elif '2' in words or 'two' in words or 'second' in words or '2nd' in words or 'kitchen' in words:
        bot_statement('You have chosen to clean the kitchen!')
    else:
        bot_statement('You chose an invalid option, please try again!')
        choose_employee_option()


bot_statement('Hello User! Could you please confirm if you are a customer, Option 1, or an employee, Option 2. To '
              'test out speech to voice, enter 3. ')

choose_user_option()



