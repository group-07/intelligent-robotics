#!/home/blanaoz/Desktop/catkin_ws/src/intelligent-robotics/venv/bin/python
# type your own virtualenv here

import os
import cv2
import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Point, Twist, PoseStamped
from math import atan2

# Reading the map and opening the binary file to write.
os.chdir(os.path.dirname(os.path.abspath(__file__)))  # change directory
print(os.getcwd())
img = cv2.imread("../data/real_data/CityMap.pgm", cv2.IMREAD_GRAYSCALE)  # call python3 file from the a_star folder
print(img)
rgb_img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

f = open("../data/real_data/CityBinaryMap.txt", "w")

# Writing the following info on the same line: height, width,
# [coordinates for the starting point inside the restaurant],
# [coordinates for each food item],
# [coordinates for the destination addresses]
# Everything is inputted manually.
f.write(str(img.shape[0]) + ' ' + str(img.shape[1])
        + ' [100, 180]'
        + ' [70, 188]' + ' [70, 197]' + ' [80, 195]' + ' [93, 196]'
        + ' [233, 67]' + ' [225, 135]' + ' [393, 191]' + ' [383, 116]' + '\n')

# Integration with ROS

x = 0.0
y = 0.0
theta = 0.0
rospy.init_node("a_star_controller")

x_estimated_pose = 0
y_estimated_pose = 0
theta_estimated_pose = 0


def newOdom1(msg):
    global x_estimated_pose
    global y_estimated_pose
    global theta_estimated_pose
    x_estimated_pose = msg.pose.position.x
    y_estimated_pose = msg.pose.position.y
    # print(x, y)
    rot_q = msg.pose.orientation
    (roll, pitch, theta_estimated_pose) = euler_from_quaternion([rot_q.x, rot_q.y, rot_q.z, rot_q.w])
    # print(str(x) + " " + str(y))


def newOdom2(msg):
    global x
    global y
    global theta
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    # print(x, y)
    rot_q = msg.pose.pose.orientation
    (roll, pitch, theta) = euler_from_quaternion([rot_q.x, rot_q.y, rot_q.z, rot_q.w])
    # print(str(x) + " " + str(y))


sub_odom = rospy.Subscriber("/odom", Odometry, newOdom2)
sub_estimated_pose = rospy.Subscriber("/estimatedpose", PoseStamped, newOdom1)
pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
r = rospy.Rate(15)

vel = Twist()


class Node:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.score = 2 ** 32
        self.parent = None
        self.valid_neighbours = []


class Graph:
    # Writing in the file: 255 (White) turns into 0 in the binary map and 0 (Black) into 1.
    # Also turn every pixel from path to red.
    @staticmethod
    def show_path_image(path: [(int, int)]):
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                if path is not None and (i, j) in path:
                    rgb_img[i][j] = (0, 0, 255)
                else:
                    if img[i][j] == 255:
                        f.write("0 ")
                    else:
                        f.write("1 ")
            f.write("\n")
        f.close()

        cv2.imshow('rgb image', rgb_img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def __init__(self, grid_width: int, grid_height: int):
        self.grid_width = grid_width
        self.grid_height = grid_height
        self.nodes = []
        self.new_img = self.map_a_star(img)
        self.fill_nodes()

    def map_a_star(self, map):
        for i in range(2, map.shape[0] - 2):
            for j in range(2, map.shape[1] - 2):
                """if map[i][j] == 255 and map[i][j + 1] == 255 and map[i + 1][j - 1] == 255 and map[i + 1][j] == 255 and map[i + 1][j + 1] == 255 and map[i + 2][j - 1] == 255 and map[i + 2][j] == 255 and map[i + 2][j + 1] == 255:
                    map[i][j] = 255
                else:
                    map[i][j] = 0
                """
                wall = False

                for ii in range(i, i + 2):
                    for jj in range(j, j + 2):
                        if img[ii][jj] != 255:
                            wall = True

                if wall:
                    map[i][j] = 0
                else:
                    map[i][j] = 255
        return map

    def fill_nodes(self):

        for i in range(self.new_img.shape[0]):
            row = []
            for j in range(self.new_img.shape[1]):
                node = Node(i, j)
                row.append(node)
            self.nodes.append(row)

        for i in range(self.new_img.shape[0]):
            for j in range(self.new_img.shape[1]):
                self.fill_valid_neighbours(self.nodes[i][j])

    def fill_valid_neighbours(self, node: "Node"):
        if self.new_img[node.x][
            node.y] == 255 and node.x > 0 and node.y > 0 and node.x < self.grid_width - 1 and node.y < self.grid_height - 1:
            if self.new_img[node.x - 1][node.y] == 255:
                node.valid_neighbours.append(self.nodes[node.x - 1][node.y])
            if self.new_img[node.x + 1][node.y] == 255:
                node.valid_neighbours.append(self.nodes[node.x + 1][node.y])
            if self.new_img[node.x][node.y - 1] == 255:
                node.valid_neighbours.append(self.nodes[node.x][node.y - 1])
            if self.new_img[node.x][node.y + 1] == 255:
                node.valid_neighbours.append(self.nodes[node.x][node.y + 1])

    def a_star(self, start: (int, int), end: (int, int)):
        self.nodes = []
        self.fill_nodes()
        start = self.nodes[start[0]][start[1]]
        end = self.nodes[end[0]][end[1]]
        start.score = 0
        later_list = [start]
        while len(later_list) > 0:
            current = later_list.pop(0)

            if current == end:
                path = [(end.x, end.y)]
                temp = end
                while temp.parent is not None:
                    temp = temp.parent
                    path.insert(0, (temp.x, temp.y))
                return path

            for adjacent_node in current.valid_neighbours:
                horizontal_distance = abs(end.x - adjacent_node.x)
                vertical_distance = abs(end.y - adjacent_node.y)
                new_score = horizontal_distance + vertical_distance
                if new_score < adjacent_node.score:
                    adjacent_node.score = new_score
                    adjacent_node.parent = current
                    index_to_add_at = 0
                    while index_to_add_at < len(later_list):
                        if adjacent_node.score < later_list[index_to_add_at].score:
                            break
                        index_to_add_at += 1
                    later_list.insert(index_to_add_at, adjacent_node)

    def run_robot(self, path):

        for pixel in path:
            real_x = (pixel[1] - 1 / 0.05) * 0.05 - 4.4
            real_y = (pixel[0] - 1 / 0.05) * 0.05 - 7.180000000000001
            print(real_y, end="\n")
            if real_y < 0:
                real_y = abs(real_y)
            else:
                real_y = -real_y

            print("Next coords: " + str(real_x) + " " + str(real_y))

            inc_x = real_x - x
            inc_y = real_y - y

            print(f"The inc coordinate x = {inc_x}")
            print(f"The inc coordinate y = {inc_y}")

            # change this to something that is not infinite
            first_theta = theta

            while True:

                inc_x = real_x - x
                inc_y = real_y - y

                print(f"The goal coordinate x = {real_x}")
                print(f"The goal coordinate y = {real_y}")
                print("")
                print(f"The robot coordinate x = {x}")
                print(f"The robot coordinate y = {y}")
                print("")
                # print(f"The inc coordinate x = {inc_x}")
                # print(f"The inc coordinate y = {inc_y}")
                # print("")
                # x in the path is actually y

                angle_to_goal = atan2(inc_y, inc_x)

                print(f"The angle to goal is = {angle_to_goal}")
                print(f"The angle of the robot is = {theta}")
                print("")
                if abs(angle_to_goal - theta) > 0.1:
                    if angle_to_goal > theta:
                        vel.linear.x = 0.0
                        vel.angular.z = 0.3
                    elif angle_to_goal < theta:
                        vel.linear.x = 0.0
                        vel.angular.z = - 0.3

                else:
                    # need to change x and y values each time
                    if abs(inc_x) < 0.1 and abs(inc_y) < 0.1:
                        # GOAL HAS BEEN REACHED
                        break
                    vel.linear.x = 0.1
                    vel.angular.z = 0.0

                pub.publish(vel)
                r.sleep()

    print("Reached here 5.")


def localise_mcl():
    for i in range(200000):
        vel.linear.x = 0.0
        vel.angular.z = 1.0
        pub.publish(vel)
    print(x_estimated_pose)
    print(y_estimated_pose)
    pixel_y = abs(int((x_estimated_pose + 1)/0.05))
    pixel_x = abs(int((y_estimated_pose + 6)/0.05))
    return pixel_x, pixel_y


def checkhouse():
    with open("../data/address.txt") as f:
        for line in f:
            pass
        last_line = line

        if last_line == "A":  # restaurant to house A
            return (166, 106), (74, 220)
        if last_line == "B":  # restaurant to house B
            return (198, 134), (127, 222)
        if last_line == "C":  # restaurant to house C
            return (166, 106), (195, 390)
        if last_line == "D":  # restaurant to house D
            return (166, 106), (115, 375)


direction = checkhouse()
print(direction)

graph1 = Graph(img.shape[0], img.shape[1])
#path = graph1.a_star(direction[0], direction[1])
#path1 = graph1.a_star((166, 106), (74, 220)) #for house A
# path2 = graph1.a_star((198, 134), (127, 222)) #for house B
# path3 = graph1.a_star((166, 106), (195, 390)) #for house C works for C with abs removed from arctan calculus and the inverse y fo real_y
# for house D
#path4 = graph1.a_star((166, 106), (115, 375))
# path_A_to_C = graph1.a_star((74, 220), (195, 390)) # works from A -> C
# path_A_to_B = graph1.a_star((74, 220), (127, 222))
# path_A_to_D = graph1.a_star((74, 220), (115, 375))
# path_B_to_A = graph1.a_star((198, 134), (74, 220))
# path_B_to_C = graph1.a_star((198, 134), (195, 390))
# path_B_to_D = graph1.a_star((198, 134), (115, 375))
# path_C_to_A = graph1.a_star((195, 390), (74, 220))
# path_C_to_B = graph1.a_star((195, 390), (127, 222))
# path_C_to_D = graph1.a_star((195, 390), (115, 375))
# path_D_to_A = graph1.a_star((115, 375), (74, 220))
# path_D_to_B = graph1.a_star((115, 375), (127, 222))
# path_D_to_C = graph1.a_star((115, 375), (195, 390))
# graph1.show_path_image(path4)

start_waypoint = localise_mcl()
print(start_waypoint)
path_mcl = graph1.a_star(start_waypoint, direction[1])

graph1.run_robot(path_mcl)
order_message_file = open("../data/order_message_file.txt", "r")
print(order_message_file.readline())
graph1.show_path_image(path_mcl)
