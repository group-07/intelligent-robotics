from geometry_msgs.msg import Pose, PoseArray, Quaternion
from .pf_base import PFLocaliserBase
import math
import rospy

from shapely.geometry import Point, Polygon

from .util import rotateQuaternion, getHeading
import random
import numpy as np

from time import time


class PFLocaliser(PFLocaliserBase):

    def __init__(self):
        # ----- Call the superclass constructor
        super(PFLocaliser, self).__init__()

        # ----- Set motion model parameters
        self.timer = 0
        self.ODOM_ROTATION_NOISE = 0  # Odometry model rotation noise
        self.ODOM_TRANSLATION_NOISE = 0  # Odometry x axis (forward) noise
        self.ODOM_DRIFT_NOISE = 0  # Odometry y axis (side-side) noise
        self.SIGMA = 0.1
        # ----- Sensor model parameters
        # empty pose initially
        self.robot_pose = Pose()
        # initial number of particles generated
        self.number_particles = 500

        self.amcl_threshold_value = 0.0001

        # best particle initialized
        self.best_particle = Pose()

        self.NUMBER_PREDICTED_READINGS = 20  # Number of readings to predict

    def initialise_particle_cloud(self, initialpose):
        """
        Set particle cloud to initialpose plus noise

        Called whenever an initialpose message is received (to change the
        starting location of the robot), or a new occupancy_map is received.
        self.particlecloud can be initialised here. Initial pose of the robot
        is also set here.

        :Args:
            | initialpose: the initial pose estimate
        :Return:
            | (geometry_msgs.msg.PoseArray) poses of the particles
        """

        self.robot_pose = initialpose
        # pose array has the poses of all particles
        pose_list = PoseArray()
        for i in range(self.number_particles):
            particle_pose = Pose()

            # calculating the noises
            # self.ODOM_TRANSLATION_NOISE = random.gauss(self.robot_pose.pose.pose.position.x, self.SIGMA)
            # self.ODOM_DRIFT_NOISE = random.gauss(self.robot_pose.pose.pose.position.y, self.SIGMA)
            # self.ODOM_ROTATION_NOISE = random.gauss(self.robot_pose.pose.pose.orientation.z, self.SIGMA)

            # generate pose between the fixed coordinate
            """random_pt = True
            random_point = Point()
            while random_pt:
                poly = Polygon([(4, -16), (16, -4.0),
                                (-4, 16), (-16, 5), (4, -16)])

                random_point = Point(random.uniform(-16, 16), random.uniform(-16, 16))
                if random_point.within(poly):
                    random_pt = False

            particle_pose.position.x = random_point.x  # random.randint(-14, 14)  #
            """
            particle_pose.position.x = self.robot_pose.pose.pose.position.x + (random.random() * random.randint(-10,10))
            particle_pose.position.x = random.gauss(particle_pose.position.x, self.SIGMA)
            # random.gauss(self.robot_pose.pose.pose.position.x, self.SIGMA)
            # self.robot_pose.pose.pose.position.x + (random.random() * random.randint(-10,10)) \
            # + self.ODOM_TRANSLATION_NOISE

            """particle_pose.position.y = random_point.y  # random.randint(-8, 8)  #"""
            particle_pose.position.y = self.robot_pose.pose.pose.position.y + (random.random() * random.randint(-10,10))
            particle_pose.position.y = random.gauss(particle_pose.position.y, self.SIGMA)
            # random.gauss(self.robot_pose.pose.pose.position.y, self.SIGMA)
            # self.robot_pose.pose.pose.position.y + (random.random() * random.randint(-10,10)) + \
            # self.ODOM_DRIFT_NOISE
            particle_pose.orientation.z = random.gauss(self.robot_pose.pose.pose.orientation.z, self.SIGMA)
            particle_pose.orientation = rotateQuaternion(self.robot_pose.pose.pose.orientation,
                                                         random.random() * math.pi * random.uniform(
                                                             -1, 1))  # rotates by a random float between (-pi, pi)

            pose_list.poses.append(particle_pose)

        # the particles should also have uniformly distributed 1/n weights

        # returning the poses of the particles
        return pose_list

    def amcl(self, pose_array, weights, threshold):
        amcl_sample = PoseArray()
        for i in range(len(weights)):
            if weights[i] >= threshold:
                amcl_sample.poses.append(pose_array.poses[i])
        self.amcl_threshold_value += 0.0001
        return amcl_sample if self.number_particles >= 200 else pose_array

    def update_particle_cloud(self, scan):
        """
        This should use the supplied laser scan to update the current
        particle cloud. i.e. self.particlecloud should be updated.

        :Args:
            | scan (sensor_msgs.msg.LaserScan): laser scan to use for update

         """
        # if moving detected then update!!!
        if self.moved:

            # normalization

            normalized_weight = [self.sensor_model.get_weight(scan, particle) for particle in self.particlecloud.poses]
            weights_sum = 0
            normalizer = sum(normalized_weight)
            for i in range(self.number_particles):
                normalized_weight[i] /= normalizer
                weights_sum += normalized_weight[i]

            new_sample = PoseArray()  # step 1 of systematic resample algo
            cumulative_distribution = [0 for _ in range(self.number_particles)]
            cumulative_distribution[0] = normalized_weight[0]
            for i in range(1, self.number_particles):
                # adding the contribution for particle i
                # if all particles are normalized then the last contribution has to be 1
                weight_particle_i = normalized_weight[i]
                cumulative_distribution[i] = cumulative_distribution[i - 1] + weight_particle_i
            # ----threshold step -----
            u = random.uniform(0.0, 1/ self.number_particles)

            i = 0
            for j in range(self.number_particles):
                while u > cumulative_distribution[i]:  # u_i > c_i
                    i += 1
                new_particle = Pose()

                # # calculating the noises
                # self.ODOM_TRANSLATION_NOISE = np.random.normal(self.particlecloud.poses[i].position.x, self.SIGMA ** 2)
                # self.ODOM_DRIFT_NOISE = np.random.normal(self.particlecloud.poses[i].position.y, self.SIGMA ** 2)
                # self.ODOM_ROTATION_NOISE = np.random.normal(self.particlecloud.poses[i].orientation.z, self.SIGMA ** 2)

                new_particle.position.x = random.gauss(self.particlecloud.poses[i].position.x, self.SIGMA)
                # rospy.loginfo(f"The noise for x is {random.gauss(self.particlecloud.poses[i].position.x, self.SIGMA)}")
                # self.particlecloud.poses[i].position.x + self.ODOM_TRANSLATION_NOISE * 0.001
                new_particle.position.y = random.gauss(self.particlecloud.poses[i].position.y, self.SIGMA)
                # rospy.loginfo(f"The noise for y is {random.gauss(self.particlecloud.poses[i].position.y, self.SIGMA)}")
                # self.particlecloud.poses[i].position.y + self.ODOM_DRIFT_NOISE * 0.001

                #new_particle.orientation = rotateQuaternion(self.particlecloud.poses[i].orientation, random.random())
                new_particle.orientation.z = random.gauss(self.particlecloud.poses[i].orientation.z, self.SIGMA)
                new_particle.orientation = rotateQuaternion(self.particlecloud.poses[i].orientation,
                                                            self.ODOM_ROTATION_NOISE)

                new_sample.poses.append(new_particle)  # picked the good particle
                u += 1 / self.number_particles

            self.best_particle = new_sample.poses[normalized_weight.index(max(normalized_weight))]


            # take comment out in order to use amcl!!!
            self.particlecloud = new_sample#self.amcl(new_sample, normalized_weight, self.amcl_threshold_value)
            self.number_particles = len(self.particlecloud.poses)
            self.particlecloud.header.frame_id = 'map'

        pass

    def estimate_pose(self):
        """
        This should calculate and return an updated robot pose estimate based
        on the particle cloud (self.particlecloud).

        Create new estimated pose, given particle cloud
        E.g. just average the location and orientation values of each of
        the particles and return this.

        Better approximations could be made by doing some simple clustering,
        e.g. taking the average location of half the particles after
        throwing away any which are outliers

        :Return:
            | (geometry_msgs.msg.Pose) robot's estimated pose.
         """

        """
        # Average of all particles 
        estimate_pose = Pose()
        position_x, position_y = 0, 0
        particles_heading = 0
        for particle in self.particlecloud.poses:
            position_x += particle.position.x
            position_y += particle.position.y
            particles_heading += particle.orientation.z

        estimate_pose.position.x = position_x / self.number_particles
        estimate_pose.position.y = position_y / self.number_particles
        estimate_pose.orientation.z = (particles_heading / self.number_particles) % math.pi

        return estimate_pose
        """
        return self.best_particle
